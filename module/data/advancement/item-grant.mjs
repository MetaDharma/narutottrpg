import JutsuConfigurationData from "./jutsu-config.mjs";

export default class ItemGrantConfigurationData extends foundry.abstract.DataModel {
  /** @inheritdoc */
  static defineSchema() {
    return {
      items: new foundry.data.fields.ArrayField(new foundry.data.fields.StringField(), {
        required: true, label: "DOCUMENT.Items"
      }),
      optional: new foundry.data.fields.BooleanField({
        required: true, label: "N5E.AdvancementItemGrantOptional", hint: "N5E.AdvancementItemGrantOptionalHint"
      }),
      jutsu: new foundry.data.fields.EmbeddedDataField(JutsuConfigurationData, {
        required: true, nullable: true, initial: null
      })
    };
  }
}