import AdvancementFlow from "./advancement-flow.mjs";

/**
 * Inline application that presents the player with an ability score improvement.
 */
export default class AbilityScoreImprovementFlow extends AdvancementFlow {

  /**
   * Player assignments to abilities.
   * @type {Object<string, number>}
   */
  assignments = {};

  /* -------------------------------------------- */

  /** @inheritdoc */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      dragDrop: [{ dropSelector: "form" }],
      template: "systems/n5e/templates/advancement/ability-score-improvement-flow.hbs"
    });
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async retainData(data) {
    await super.retainData(data);
    this.assignments = this.retainedData.assignments ?? {};
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async getData() {
    const points = {
      assigned: Object.keys(CONFIG.N5E.abilities).reduce((assigned, key) => {
        if ( !this.advancement.canImprove(key) || this.advancement.configuration.fixed[key] ) return assigned;
        return assigned + (this.assignments[key] ?? 0);
      }, 0),
      total: this.advancement.configuration.points
    };
    points.available = points.total - points.assigned;

    const formatter = new Intl.NumberFormat(game.i18n.lang, { signDisplay: "always" });

    const abilities = Object.entries(CONFIG.N5E.abilities).reduce((obj, [key, data]) => {
      if ( !this.advancement.canImprove(key) ) return obj;
      const ability = this.advancement.actor.system.abilities[key];
      const fixed = this.advancement.configuration.fixed[key] ?? 0;
      const value = Math.min(ability.value + ((fixed || this.assignments[key]) ?? 0), ability.max);
      const max = fixed ? value : Math.min(value + points.available, ability.max);
      obj[key] = {
        key, max, value,
        name: `abilities.${key}`,
        label: data.label,
        initial: ability.value,
        min: fixed ? max : ability.value,
        delta: (value - ability.value) ? formatter.format(value - ability.value) : null,
        showDelta: true,
        isFixed: !!fixed,
        canIncrease: (value < max) && !fixed,
        canDecrease: (value > ability.value) && !fixed
      };
      return obj;
    }, {});

    const pluralRule = new Intl.PluralRules(game.i18n.lang).select(points.available);
    return foundry.utils.mergeObject(super.getData(), {
      abilities, points,
      staticIncrease: !this.advancement.configuration.points,
      pointsRemaining: game.i18n.format(
        `N5E.AdvancementAbilityScoreImprovementPointsRemaining.${pluralRule}`, {points: points.available}
      )
    });
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  activateListeners(html) {
    super.activateListeners(html);
    html.find(".adjustment-button").click(this._onClickButton.bind(this));
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onChangeInput(event) {
    super._onChangeInput(event);
    const input = event.currentTarget;
    const key = input.closest("[data-score]").dataset.score;
    const clampedValue = Math.clamped(input.valueAsNumber, Number(input.min), Number(input.max));
    this.assignments[key] = clampedValue - Number(input.dataset.initial);
    this.render();
  }

  /* -------------------------------------------- */

  /**
   * Handle clicking the plus and minus buttons.
   * @param {Event} event  Triggering click event.
   */
  _onClickButton(event) {
    event.preventDefault();
    const action = event.currentTarget.dataset.action;
    const key = event.currentTarget.closest("li").dataset.score;

    this.assignments[key] ??= 0;
    if ( action === "decrease" ) this.assignments[key] -= 1;
    else if ( action === "increase" ) this.assignments[key] += 1;
    else return;

    this.render();
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async _updateObject(event, formData) {
    await this.advancement.apply(this.level, {
      assignments: this.assignments,
    });
  }
}