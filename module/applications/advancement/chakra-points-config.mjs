import AdvancementConfig from "./advancement-config.mjs";

/**
 * Configuration application for chakra points.
 */
export default class ChakraPointsConfig extends AdvancementConfig {

  /** @inheritdoc */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      template: "systems/n5e/templates/advancement/chakra-points-config.hbs"
    });
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  getData() {
    return foundry.utils.mergeObject(super.getData(), {
      chakraDie: this.advancement.chakraDie
    });
  }
}