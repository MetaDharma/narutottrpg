import * as advancement from "./documents/advancement/_module.mjs";
import { preLocalize } from "./utils.mjs";

// Namespace Configuration Values
const N5E = {};

// ASCII Artwork
N5E.ASCII = `_______________________________
 _   _                      _          
| \\ | |  __ _  _ __  _   _ | |_   ___  
|  \\| | / _' || '__|| | | || __| / _ \\ 
| |\\  || (_| || |   | |_| || |_ | (_) |
|_| \\_| \\__,_||_|    \\__,_| \\__| \\___/ 
_______________________________`;

/**
 * Configuration data for abilities.
 *
 * @typedef {object} AbilityConfiguration
 * @property {string} label                               Localized label.
 * @property {string} abbreviation                        Localized abbreviation.
 * @property {string} [type]                              Whether this is a "physical" or "mental" ability.
 * @property {Object<string, number|string>}  [defaults]  Default values for this ability based on actor type.
 *                                                        If a string is used, the system will attempt to fetch.
 *                                                        the value of the specified ability.
 */

/**
 * The set of Ability Scores used within the system.
 * @enum {AbilityConfiguration}
 */
N5E.abilities = {
  str: {
    label: "N5E.AbilityStr",
    abbreviation: "N5E.AbilityStrAbbr",
    type: "physical"
  },
  dex: {
    label: "N5E.AbilityDex",
    abbreviation: "N5E.AbilityDexAbbr",
    type: "physical"
  },
  con: {
    label: "N5E.AbilityCon",
    abbreviation: "N5E.AbilityConAbbr",
    type: "physical"
  },
  int: {
    label: "N5E.AbilityInt",
    abbreviation: "N5E.AbilityIntAbbr",
    type: "mental",
    defaults: { vehicle: 0 }
  },
  wis: {
    label: "N5E.AbilityWis",
    abbreviation: "N5E.AbilityWisAbbr",
    type: "mental",
    defaults: { vehicle: 0 }
  },
  cha: {
    label: "N5E.AbilityCha",
    abbreviation: "N5E.AbilityChaAbbr",
    type: "mental",
    defaults: { vehicle: 0 }
  },
  hon: {
    label: "N5E.AbilityHon",
    abbreviation: "N5E.AbilityHonAbbr",
    type: "mental",
    defaults: { npc: "cha", vehicle: 0 },
    improvement: false
  },
  san: {
    label: "N5E.AbilitySan",
    abbreviation: "N5E.AbilitySanAbbr",
    type: "mental",
    defaults: { npc: "wis", vehicle: 0 },
    improvement: false
  }
};
preLocalize("abilities", { keys: ["label", "abbreviation"] });
patchConfig("abilities", "label", { since: 2.2, until: 2.4 });

Object.defineProperty(N5E, "abilityAbbreviations", {
  get() {
    foundry.utils.logCompatibilityWarning(
      "The `abilityAbbreviations` configuration object has been merged with `abilities`.",
      { since: "N5E 2.2", until: "N5E 2.4" }
    );
    return Object.fromEntries(Object.entries(N5E.abilities).map(([k, v]) => [k, v.abbreviation]));
  }
});

/**
 * Configure which ability score is used as the default modifier for initiative rolls.
 * @type {string}
 */
N5E.initiativeAbility = "dex";

/**
 * Configure which ability score is used when calculating chakra points per level.
 * @type {string}
 */
N5E.chakraPointsAbility = "con";

/**
 * Configure which ability score is used when calculating hit points per level.
 * @type {string}
 */
N5E.hitPointsAbility = "con";

/* -------------------------------------------- */

/**
 * Configuration data for skills.
 *
 * @typedef {object} SkillConfiguration
 * @property {string} label    Localized label.
 * @property {string} ability  Key for the default ability used by this skill.
 */

/**
 * The set of skill which can be trained with their default ability scores.
 * @enum {SkillConfiguration}
 */
N5E.skills = {
  acr: { label: "N5E.SkillAcr", ability: "dex" },
  ani: { label: "N5E.SkillAni", ability: "wis" },
  ath: { label: "N5E.SkillAth", ability: "str" },
  ctl: { label: "N5E.SkillCtl", ability: "con" },
  cra: { label: "N5E.SkillCra", ability: "int" },
  dec: { label: "N5E.SkillDec", ability: "cha" },
  gen: { label: "N5E.SkillGen", ability: "wis" },
  his: { label: "N5E.SkillHis", ability: "int" },
  ins: { label: "N5E.SkillIns", ability: "wis" },
  itm: { label: "N5E.SkillItm", ability: "cha" },
  inv: { label: "N5E.SkillInv", ability: "int" },
  med: { label: "N5E.SkillMed", ability: "wis" },
  nat: { label: "N5E.SkillNat", ability: "int" },
  nin: { label: "N5E.SkillNin", ability: "int" },
  prc: { label: "N5E.SkillPrc", ability: "wis" },
  prf: { label: "N5E.SkillPrf", ability: "cha" },
  per: { label: "N5E.SkillPer", ability: "cha" },
  slt: { label: "N5E.SkillSlt", ability: "dex" },
  ste: { label: "N5E.SkillSte", ability: "dex" },
  sur: { label: "N5E.SkillSur", ability: "wis" },
  tai: { label: "N5E.SkillTai", ability: "str" },
};
preLocalize("skills", { key: "label", sort: true });

/* -------------------------------------------- */

/**
 * Character alignment options.
 * @enum {string}
 */
N5E.alignments = {
  lg: "N5E.AlignmentLG",
  ng: "N5E.AlignmentNG",
  cg: "N5E.AlignmentCG",
  ln: "N5E.AlignmentLN",
  tn: "N5E.AlignmentTN",
  cn: "N5E.AlignmentCN",
  le: "N5E.AlignmentLE",
  ne: "N5E.AlignmentNE",
  ce: "N5E.AlignmentCE"
};
preLocalize("alignments");

/* -------------------------------------------- */

/**
 * An enumeration of item attunement types.
 * @enum {number}
 */
N5E.attunementTypes = {
  NONE: 0,
  REQUIRED: 1,
  ATTUNED: 2
};

/**
 * An enumeration of item attunement states.
 * @type {{"0": string, "1": string, "2": string}}
 */
N5E.attunements = {
  0: "N5E.AttunementNone",
  1: "N5E.AttunementRequired",
  2: "N5E.AttunementAttuned"
};
preLocalize("attunements");

/* -------------------------------------------- */

/**
 * General weapon categories.
 * @enum {string}
 */
N5E.weaponProficiencies = {
  sim: "N5E.WeaponSimpleProficiency",
  mar: "N5E.WeaponMartialProficiency"
};
preLocalize("weaponProficiencies");

/**
 * A mapping between `N5E.weaponTypes` and `N5E.weaponProficiencies` that
 * is used to determine if character has proficiency when adding an item.
 * @enum {(boolean|string)}
 */
N5E.weaponProficienciesMap = {
  simpleM: "sim",
  simpleR: "sim",
  martialM: "mar",
  martialR: "mar"
};

/**
 * The basic weapon types in N5e. This enables specific weapon proficiencies or
 * starting equipment provided by classes and backgrounds.
 * @enum {string}
 */
N5E.weaponIds = {
  battlewire: "Kvcec8gTpLA6okzc",
  blowgun: "AOghue2SZutEyA3I",
  broadsword: "vFCupNXXefo9b3EB",
  chainedhandscythe: "LrZEtQG9uFI1ezcM",
  chainedspear: "aWeaNaEOR4gdTLNR",
  chakram: "zY1B6engJkZo6Etj",
  combatbracers: "2TSvXxIspNZc4jBg",
  fumashuriken: "Uw6x4WIOiEo4DOkh",
  greataxe: "1e43ylbZk6NDcYxj",
  handaxe: "54jFw7GUHC9RdJy6",
  handcrossbow: "3NqF5WJw7iib6FIw",
  handscythe: "as1PKzt80nDDkbV9",
  heavycrossbow: "z2JoFgWmel77ak8m",
  hookedlance: "jJCdF8OVHgIgBHIo",
  ironclaw: "51BeNvJhvvk139DB",
  jitte: "5kgl3O9AJJFs5oUY",
  katana: "8DZmrja5PoL3HS4Y",
  knuckleblades: "cAxFZCT316pOJDi7",
  kunai: "0qfJxwsoGKmjWroU",
  longbow: "GR635YyHtHLyxr1n",
  monsterchakram: "UTqkVvFqvnaP3W3z",
  monstershuriken: "4LvtkWFj4B0i5Wvn",
  naginata: "UENgyOUMjivITU2V",
  net: "PgEOfO8M0w2CuKnV",
  nunchaku: "r6REb0t6jdTnIgsN",
  odachi: "L5uRnyIYf7t9URuA",
  quarterstaff: "tKP354Nuba8DUDeb",
  sasumata: "GY22192uvUeWZkZ9",
  scythe: "g0ADSaJe8KF3nPT6",
  senbon: "FMhx5c9Ld1CLv3Sd",
  shortbow: "LYr4nhiu5W3dwuqP",
  shuriken: "rjlsdpnHENtJYDIW",
  sling: "sVfuJek4n0uGUStk",
  spear: "hjsIwIyJzwQqJPLB",
  tanto: "C2OZQBvTg1gg0JUL",
  tetsubo: "IJ62fQqvRFJihtKv",
  tonfa: "CNEh0wfp4UcLmTwh",
  warclub: "tnUkXeD1pyLX2Foa",
  weightedchain: "OiCIkbNA4mXFncOv",
  whip: "ToVRF4gJATegT1K7"
};

/* -------------------------------------------- */

/**
 * The basic ammunition types.
 * @enum {string}
 */
N5E.ammoIds = {
  arrow: "3c7JXOzsv55gqJS5",
  blowgunNeedle: "gBQ8xqTA5f8wP5iu",
  crossbowBolt: "SItCnYBqhzqBoaWG"
};

/* -------------------------------------------- */

/**
 * The categories into which Tool items can be grouped.
 *
 * @enum {string}
 */
N5E.toolTypes = {
  art: "N5E.ToolArtisans",
  game: "N5E.ToolGamingSet",
  music: "N5E.ToolMusicalInstrument"
};
preLocalize("toolTypes", { sort: true });

/**
 * The categories of tool proficiencies that a character can gain.
 *
 * @enum {string}
 */
N5E.toolProficiencies = {
  ...N5E.toolTypes,
  vehicle: "N5E.ToolVehicle"
};
preLocalize("toolProficiencies", { sort: true });

/**
 * The basic tool types in N5e. This enables specific tool proficiencies or
 * starting equipment provided by classes and backgrounds.
 * @enum {string}
 */
N5E.toolIds = {
  alchemist: "pQ98T4NPfzwRbvEN",
  armorsmith: "qfPv8iwIRTX7uFIK",
  cooking: "OhTgffwy9GJA1vpW",
  demolitions: "s8fcPF5DgTmtE692",
  disguise: "ZzXFeio6MA3SYR52",
  forensics: "thfe5qSt4hObhzYe",
  forgery: "64hk2LuiCGMn3q1F",
  hackers: "Y0EMX4UIrmEpOizX",
  medicine: "0L4z8zujqROi3Zt6",
  poison: "oy5TwFsUozcHDuPC",
  security: "1wL1rg3l6hasmVKG",
  trappers: "Vh5SG6pAQLQUhbaB",
  weaponsmith: "01sczB8UOLoc0cXR"
};

/* -------------------------------------------- */

/**
 * Time periods that accept a numeric value.
 * @enum {string}
 */
N5E.scalarTimePeriods = {
  turn: "N5E.TimeTurn",
  round: "N5E.TimeRound",
  minute: "N5E.TimeMinute",
  hour: "N5E.TimeHour",
  day: "N5E.TimeDay",
  month: "N5E.TimeMonth",
  year: "N5E.TimeYear"
};
preLocalize("scalarTimePeriods");

/* -------------------------------------------- */

/**
 * Time periods for jutsu that don't have a defined ending.
 * @enum {string}
 */
N5E.permanentTimePeriods = {
  disp: "N5E.TimeDisp",
  dstr: "N5E.TimeDispTrig",
  perm: "N5E.TimePerm"
};
preLocalize("permanentTimePeriods");

/* -------------------------------------------- */

/**
 * Time periods that don't accept a numeric value.
 * @enum {string}
 */
N5E.specialTimePeriods = {
  inst: "N5E.TimeInst",
  spec: "N5E.Special"
};
preLocalize("specialTimePeriods");

/* -------------------------------------------- */

/**
 * The various lengths of time over which effects can occur.
 * @enum {string}
 */
N5E.timePeriods = {
  ...N5E.specialTimePeriods,
  ...N5E.permanentTimePeriods,
  ...N5E.scalarTimePeriods
};
preLocalize("timePeriods");

/* -------------------------------------------- */

/**
 * Various ways in which an item or ability can be activated.
 * @enum {string}
 */
N5E.abilityActivationTypes = {
  action: "N5E.Action",
  bonus: "N5E.BonusAction",
  reaction: "N5E.Reaction",
  full: "N5E.FullAction",
  minute: N5E.timePeriods.minute,
  hour: N5E.timePeriods.hour,
  day: N5E.timePeriods.day,
  special: N5E.timePeriods.spec,
  elite: "N5E.EliteActionLabel",
  crew: "N5E.VehicleCrewAction"
};
preLocalize("abilityActivationTypes");

/* -------------------------------------------- */

/**
 * Different things that an ability can consume upon use.
 * @enum {string}
 */
N5E.abilityConsumptionTypes = {
  ammo: "N5E.ConsumeAmmunition",
  attribute: "N5E.ConsumeAttribute",
  chakraDice: "N5E.ConsumeChakraDice",
  hitDice: "N5E.ConsumeHitDice",
  material: "N5E.ConsumeMaterial",
  charges: "N5E.ConsumeCharges"
};
preLocalize("abilityConsumptionTypes", { sort: true });

/* -------------------------------------------- */

/**
 * Different things that an ability can consume upon use.
 * @enum {string}
 */
 N5E.chakraConsumptionTypes = {
  attribute: "N5E.ConsumeAttribute"
};
preLocalize("chakraConsumptionTypes", { sort: true });

/* -------------------------------------------- */

/**
 * Creature sizes.
 * @enum {string}
 */
N5E.actorSizes = {
  tiny: "N5E.SizeTiny",
  sm: "N5E.SizeSmall",
  med: "N5E.SizeMedium",
  lg: "N5E.SizeLarge",
  huge: "N5E.SizeHuge",
  grg: "N5E.SizeGargantuan",
  csl: "N5E.SizeColossal"
};
preLocalize("actorSizes");

/**
 * Creature ranks.
 * @enum {string}
 */
N5E.actorRanks = {
  0: "N5E.CreatureRank0",
  1: "N5E.CreatureRank1",
  2: "N5E.CreatureRank2",
  3: "N5E.CreatureRank3",
  4: "N5E.CreatureRank4",
  5: "N5E.CreatureRank5"
};
preLocalize("actorRanks");

/**
 * Default token image size for the values of `N5E.actorSizes`.
 * @enum {number}
 */
N5E.tokenSizes = {
  tiny: 0.5,
  sm: 1,
  med: 1,
  lg: 2,
  huge: 3,
  grg: 4,
  csl: 5
};

/**
 * Colors used to visualize temporary and temporary maximum CP in token chakra bars.
 * @enum {number}
 */
N5E.tokenCPColors = {
  damage: 0xFF0000,
  healing: 0x00FF00,
  temp: 0x66CCFF,
  tempmax: 0x440066,
  negmax: 0x550000
};

/**
 * Colors used to visualize temporary and temporary maximum HP in token health bars.
 * @enum {number}
 */
N5E.tokenHPColors = {
  damage: 0xFF0000,
  healing: 0x00FF00,
  temp: 0x66CCFF,
  tempmax: 0x440066,
  negmax: 0x550000
};

/* -------------------------------------------- */

/**
 * Default types of creatures.
 * *Note: Not pre-localized to allow for easy fetching of pluralized forms.*
 * @enum {string}
 */
N5E.creatureTypes = {
  beast: "N5E.CreatureBeast",
  construct: "N5E.CreatureConstruct",
  demon: "N5E.CreatureDemon",
  humanoid: "N5E.CreatureHumanoid",
  monstrosity: "N5E.CreatureMonstrosity",
  plant: "N5E.CreaturePlant",
  sage: "N5E.CreatureSage",
  undead: "N5E.CreatureUndead"
};

/* -------------------------------------------- */

/**
 * Classification types for item action types.
 * @enum {string}
 */
N5E.itemActionTypes = {
  mwak: "N5E.ActionMWAK",
  rwak: "N5E.ActionRWAK",
  mnak: "N5E.ActionMNAK",
  rnak: "N5E.ActionRNAK",
  mgak: "N5E.ActionMGAK",
  rgak: "N5E.ActionRGAK",
  mtak: "N5E.ActionMTAK",
  rtak: "N5E.ActionRTAK",
  save: "N5E.ActionSave",
  abil: "N5E.ActionAbil",
  heal: "N5E.ActionHeal",
  util: "N5E.ActionUtil",
  other: "N5E.ActionOther"
};
preLocalize("itemActionTypes");

/* -------------------------------------------- */

/**
 * Different ways in which item capacity can be limited.
 * @enum {string}
 */
N5E.itemCapacityTypes = {
  items: "N5E.ItemContainerCapacityItems",
  bulk: "N5E.ItemContainerCapacityBulk"
};
preLocalize("itemCapacityTypes", { sort: true });

/* -------------------------------------------- */

/**
 * List of various item rarities.
 * @enum {string}
 */
N5E.itemRarity = {
  0: "N5E.ItemRarity0",
  1: "N5E.ItemRarity1",
  2: "N5E.ItemRarity2",
  3: "N5E.ItemRarity3",
  4: "N5E.ItemRarity4"
};
preLocalize("itemRarity");

/* -------------------------------------------- */

/**
 * Enumerate the lengths of time over which an item can have limited use ability.
 * @enum {string}
 */
N5E.limitedUsePeriods = {
  sr: "N5E.ShortRest",
  lr: "N5E.LongRest",
  day: "N5E.Day",
  charges: "N5E.Charges"
};
preLocalize("limitedUsePeriods");

/* -------------------------------------------- */

/**
 * Specific equipment types that modify base AC.
 * @enum {string}
 */
N5E.armorTypes = {
  light: "N5E.EquipmentLight",
  medium: "N5E.EquipmentMedium",
  heavy: "N5E.EquipmentHeavy",
  natural: "N5E.EquipmentNatural"
};
preLocalize("armorTypes");

/* -------------------------------------------- */

/**
 * Equipment types that aren't armor.
 * @enum {string}
 */
N5E.miscEquipmentTypes = {
  clothing: "N5E.EquipmentClothing",
  trinket: "N5E.EquipmentTrinket",
  vehicle: "N5E.EquipmentVehicle"
};
preLocalize("miscEquipmentTypes", { sort: true });

/* -------------------------------------------- */

/**
 * The set of equipment types for armor, clothing, and other objects which can be worn by the character.
 * @enum {string}
 */
N5E.equipmentTypes = {
  ...N5E.miscEquipmentTypes,
  ...N5E.armorTypes
};
preLocalize("equipmentTypes", { sort: true });

/* -------------------------------------------- */

/**
 * The various types of vehicles in which characters can be proficient.
 * @enum {string}
 */
N5E.vehicleTypes = {
  air: "N5E.VehicleTypeAir",
  land: "N5E.VehicleTypeLand",
  water: "N5E.VehicleTypeWater"
};
preLocalize("vehicleTypes", { sort: true });

/* -------------------------------------------- */

/**
 * The set of Armor Proficiencies which a character may have.
 * @type {object}
 */
N5E.armorProficiencies = {
  lgt: N5E.equipmentTypes.light,
  med: N5E.equipmentTypes.medium,
  hvy: N5E.equipmentTypes.heavy
};
preLocalize("armorProficiencies");

/**
 * A mapping between `N5E.equipmentTypes` and `N5E.armorProficiencies` that
 * is used to determine if character has proficiency when adding an item.
 * @enum {(boolean|string)}
 */
N5E.armorProficienciesMap = {
  natural: true,
  clothing: true,
  light: "lgt",
  medium: "med",
  heavy: "hvy"
};

/**
 * The basic armor types in N5e. This enables specific armor proficiencies,
 * automated AC calculation in NPCs, and starting equipment.
 * @enum {string}
 */
N5E.armorIds = {
  armoredcombat: "UK8iBXJMllaqCOWD",
  armoredflak: "YmUvKZtnLZMpiemi",
  armoredjonin: "bXzNWcALqn8INPek",
  battle: "kA4NzO8XnnVflXbc",
  chunin: "rnKSHecr3PQfJbaW",
  combat: "FHcWNAcagETurhmu",
  flak: "5CJe1JL0If4QGEVy",
  jonin: "ipaEwDwowhsR6B3Q",
  leather: "5S6ELiMEVcJ9s220",
  padded: "TOYUJoV8heNml5fN",
  reinforcedchunin: "ZBpY86faKuMysDwH",
  ronin: "BqjKlxcamhpG6iII",
  samurai: "OUNNWGCjkGDew1Kt",
  shinobibattle: "K0Q7woaFhMBaWcjP",
  studded: "tarzoHNMgrS9Q6qY"
};

/**
 * Common armor class calculations.
 * @enum {{ label: string, [formula]: string }}
 */
N5E.armorClasses = {
  flat: {
    label: "N5E.ArmorClassFlat",
    formula: "@attributes.ac.flat"
  },
  natural: {
    label: "N5E.ArmorClassNatural",
    formula: "@attributes.ac.flat"
  },
  default: {
    label: "N5E.ArmorClassEquipment",
    formula: "@attributes.ac.armor + @attributes.ac.dex + floor(@prof/2)"
  },
  chakra: {
    label: "N5E.ArmorClassChakra",
    formula: "13 + @abilities.dex.mod + floor(@prof/2)"
  },
  hoshigaki: {
    label: "N5E.ArmorClassHoshigaki",
    formula: "10 + @abilities.con.mod + floor(@prof/2)"
  },
  sharingan: {
    label: "N5E.ArmorClassSharingan",
    formula: "10 + @abilities.dex.mod + @abilities.int.mod + floor(@prof/2)"
  },
  mirage: {
    label: "N5E.ArmorClassMirage",
    formula: "10 + @abilities.wis.mod + floor(@prof/2)"
  },
  unarmoredTai: {
    label: "N5E.ArmorClassUnarmoredTaijutsu",
    formula: "10 + @abilities.dex.mod + @abilities.wis.mod + floor(@prof/2)"
  },
  herbalist: {
    label: "N5E.ArmorClassHerbalist",
    formula: "15 + @prof"
  },
  custom: {
    label: "N5E.ArmorClassCustom"
  }
};
preLocalize("armorClasses", { key: "label" });

/* -------------------------------------------- */

/**
 * Enumerate the valid consumable types which are recognized by the system.
 * @enum {string}
 */
N5E.consumableTypes = {
  ammo: "N5E.ConsumableAmmunition",
  pill: "N5E.ConsumablePill",
  poison: "N5E.ConsumablePoison",
  food: "N5E.ConsumableFood",
  scroll: "N5E.ConsumableScroll",
  trinket: "N5E.ConsumableTrinket"
};
preLocalize("consumableTypes", { sort: true });

/* -------------------------------------------- */

/**
 * Types of containers.
 * @enum {string}
 */
N5E.containerTypes = {
  backpack: "H8YCd689ezlD26aT",
  barrel: "7Yqbqg5EtVW16wfT",
  basket: "Wv7HzD6dv1P0q78N",
  boltcase: "eJtPBiZtr2pp6ynt",
  bottle: "HZp69hhyNZUUCipF",
  bucket: "mQVYcHmMSoCUnBnM",
  case: "5mIeX824uMklU3xq",
  chest: "2YbuclKfhDL0bU4u",
  flask: "lHS63sC6bypENNlR",
  jug: "0ZBWwjFz3nIAXMLW",
  pot: "M8xM8BLK4tpUayEE",
  pitcher: "nXWdGtzi8DXDLLsL",
  pouch: "9bWTRRDym06PzSAf",
  quiver: "4MtQKPn9qMWCFjDA",
  sack: "CNdDj8dsXVpRVpXt",
  saddlebags: "TmfaFUSZJAotndn9",
  tankard: "uw6fINSmZ2j2o57A",
  vial: "meJEfX3gZgtMX4x2"
};

/* -------------------------------------------- */
/**
 * Configuration data for an item with the "feature" type.
 *
 * @typedef {object} FeatureTypeConfiguration
 * @property {string} label                       Localized label for this type.
 * @property {Object<string, string>} [subtypes]  Enum containing localized labels for subtypes.
 */

/**
 * Types of "features" items.
 * @enum {FeatureTypeConfiguration}
 */
N5E.featureTypes = {
  background: {
    label: "N5E.Feature.Background"
  },
  class: {
    label: "N5E.Feature.Class",
    subtypes: {
      malleableMirages: "N5E.ClassFeature.MalleableMirages",
      genjutsuInception: "N5E.ClassFeature.GenjutsuInception",
      realWorldConversion: "N5E.ClassFeature.RealWorldConversion",
      masterOfIllusion: "N5E.ClassFeature.MasterOfIllusion",
      huntersPattern: "N5E.ClassFeature.HuntersPattern",
      huntersExploit: "N5E.ClassFeature.HuntersExploit",
      defensiveTactic: "N5E.ClassFeature.DefensiveTactic",
      wardenWeaponProperty: "N5E.ClassFeature.WardenWeaponProperty",
      medicalAssassinationTechnique: "N5E.ClassFeature.MedicalAssassinationTechnique",
      shadowAssassinationTechnique: "N5E.ClassFeature.ShadowAssassinationTechnique",
      toolsoftheTrade: "N5E.ClassFeature.ToolsoftheTrade",
      toxicAssassinationTechnique: "N5E.ClassFeature.ToxicAssassinationTechnique",
      viceAssassinationTechnique: "N5E.ClassFeature.ViceAssassinationTechnique",
      voidAssassinationTechnique: "N5E.ClassFeature.VoidAssassinationTechnique",
      prostheticAttachment: "N5E.ClassFeature.ProstheticAttachment",
      wolfTechnique: "N5E.ClassFeature.WolfTechnique",
      masterPlan: "N5E.ClassFeature.MasterPlan",
      braveOrder: "N5E.ClassFeature.BraveOrder",
      medicalDoctrine: "N5E.ClassFeature.MedicalDoctrine",
      fightingStance: "N5E.ClassFeature.FightingStance",
      shinobiAdept: "N5E.ClassFeature.ShinobiAdept",
      jackOfAllMasterOfNone: "N5E.ClassFeature.JackOfAllMasterOfNone",
      signatureJutsu: "N5E.ClassFeature.SignatureJutsu",
      signatureTechnique: "N5E.ClassFeature.SignatureTechnique",
      efficientMolding: "N5E.ClassFeature.EfficientMolding",
      arbiterManeuver: "N5E.ClassFeature.ArbiterManeuver",
      cloningManeuver: "N5E.ClassFeature.CloningManeuver",
      defensiveManeuver: "N5E.ClassFeature.DefensiveManeuver",
      elementalManeuver: "N5E.ClassFeature.ElementalManeuver",
      pathfinderManeuver: "N5E.ClassFeature.PathfinderManeuver",
      phantomManeuver: "N5E.ClassFeature.PhantomManeuver",
      tacticalManeuver: "N5E.ClassFeature.TacticalManeuver",
      tricksterManeuver: "N5E.ClassFeature.TricksterManeuver",
      martialTechnique: "N5E.ClassFeature.MartialTechnique",
      natureEnhancedCombat: "N5E.ClassFeature.NatureEnhancedCombat",
      elementalCombat: "N5E.ClassFeature.ElementalCombat",
      elementalArmor: "N5E.ClassFeature.ElementalArmor",
      wrathOfNature: "N5E.ClassFeature.WrathOfNature",
      handWrapsOfPassion: "N5E.ClassFeature.HandWrapsOfPassion",
      superiorWeaponFlurry: "N5E.ClassFeature.SuperiorWeaponFlurry",
      battleDancerStyle: "N5E.ClassFeature.BattleDancerStyle",
      gungnirPiercerStyle: "N5E.ClassFeature.GungnirPiercerStyle",
      obsidianHammerStyle: "N5E.ClassFeature.ObsidianHammerStyle",
      phantomBladeStyle: "N5E.ClassFeature.PhantomBladeStyle",
      primalWeaponStyle: "N5E.ClassFeature.PrimalWeaponStyle",
      rangerStyle: "N5E.ClassFeature.RangerStyle",
      samuraiStyle: "N5E.ClassFeature.SamuraiStyle",
      slayerStyle: "N5E.ClassFeature.SlayerStyle",
      puppeteerAugment: "N5E.ClassFeature.PuppeteerAugment",
      puppetChassis: "N5E.ClassFeature.PuppetChassis",
      juggernautAugment: "N5E.ClassFeature.JuggernautAugment",
      wanderingAroma: "N5E.ClassFeature.WanderingAroma",
      bonusToolInfusion: "N5E.ClassFeature.BonusToolInfusion"
    }
  },
  adversary: {
    label: "N5E.Feature.Adversary"
  },
  clan: {
    label: "N5E.Feature.Clan"
  },
  classmod: {
    label: "N5E.Feature.ClassMod"
  },
  feat: {
    label: "N5E.Feature.Feat"
  }
};
preLocalize("featureTypes", { key: "label" });
preLocalize("featureTypes.class.subtypes", { sort: true });

/* -------------------------------------------- */

/**
 * @typedef {object} CurrencyConfiguration
 * @property {string} label         Localized label for the currency.
 * @property {string} abbreviation  Localized abbreviation for the currency.
 */

/**
 * The valid currency denominations with localized labels, abbreviations, and conversions.
 * @enum {CurrencyConfiguration}
 */
N5E.currencies = {
  ryo: {
    label: "N5E.CurrencyRyo",
    abbreviation: "N5E.CurrencyAbbrRyo"
  }
};
preLocalize("currencies", { keys: ["label", "abbreviation"] });

/* -------------------------------------------- */
/*  Damage Types                                */
/* -------------------------------------------- */

/**
 * Types of damage that are considered physical.
 * @enum {string}
 */
N5E.physicalDamageTypes = {
  bludgeoning: "N5E.DamageBludgeoning",
  piercing: "N5E.DamagePiercing",
  slashing: "N5E.DamageSlashing"
};
preLocalize("physicalDamageTypes", { sort: true });

/* -------------------------------------------- */

/**
 * Types of damage the can be caused by abilities.
 * @enum {string}
 */
N5E.damageTypes = {
  ...N5E.physicalDamageTypes,
  acid: "N5E.DamageAcid",
  bludgeoning: "N5E.DamageBludgeoning",
  cold: "N5E.DamageCold",
  chakra: "N5E.DamageChakra",
  earth: "N5E.DamageEarth",
  fire: "N5E.DamageFire",
  force: "N5E.DamageForce",
  lightning: "N5E.DamageLightning",
  necrotic: "N5E.DamageNecrotic",
  piercing: "N5E.DamagePiercing",
  poison: "N5E.DamagePoison",
  psychic: "N5E.DamagePsychic",
  slashing: "N5E.DamageSlashing",
  sonic: "N5E.DamageSonic",
  wind: "N5E.DamageWind"
};
preLocalize("damageTypes", { sort: true });

/* -------------------------------------------- */

/**
 * Types of damage to which an actor can possess resistance, immunity, or vulnerability.
 * @enum {string}
 * @deprecated
 */
N5E.damageResistanceTypes = {
  ...N5E.damageTypes,
  physical: "N5E.DamagePhysical"
};
preLocalize("damageResistanceTypes", { sort: true });

/* -------------------------------------------- */
/*  Movement                                    */
/* -------------------------------------------- */

/**
 * Different types of healing that can be applied using abilities.
 * @enum {string}
 */
N5E.healingTypes = {
  healing: "N5E.Healing",
  temphp: "N5E.HealingTemp",
  amping:"N5E.Amping",
  tempcp:"N5E.AmpingTemp"
};
preLocalize("healingTypes");

/* -------------------------------------------- */

/**
 * The valid units of measure for movement distances in the game system.
 * By default this uses the imperial units of feet and miles.
 * @enum {string}
 */
N5E.movementTypes = {
  burrow: "N5E.MovementBurrow",
  climb: "N5E.MovementClimb",
  fly: "N5E.MovementFly",
  swim: "N5E.MovementSwim",
  walk: "N5E.MovementWalk"
};
preLocalize("movementTypes", { sort: true });

/* -------------------------------------------- */
/*  Measurement                                 */
/* -------------------------------------------- */

/**
 * The valid units of measure for movement distances in the game system.
 * By default this uses the imperial units of feet and miles.
 * @enum {string}
 */
N5E.movementUnits = {
  ft: "N5E.DistFt",
  mi: "N5E.DistMi",
  m: "N5E.DistM",
  km: "N5E.DistKm"
};
preLocalize("movementUnits");

/* -------------------------------------------- */

/**
 * The types of range that are used for measuring actions and effects.
 * @enum {string}
 */
N5E.rangeTypes = {
  self: "N5E.DistSelf",
  touch: "N5E.DistTouch",
  spec: "N5E.Special",
  any: "N5E.DistAny"
};
preLocalize("rangeTypes");

/* -------------------------------------------- */

/**
 * The valid units of measure for the range of an action or effect. A combination of `N5E.movementUnits` and
 * `N5E.rangeUnits`.
 * @enum {string}
 */
N5E.distanceUnits = {
  ...N5E.movementUnits,
  ...N5E.rangeTypes
};
preLocalize("distanceUnits");

/* -------------------------------------------- */
/*  Targeting                                   */
/* -------------------------------------------- */

/**
 * Targeting types that apply to one or more distinct targets.
 * @enum {string}
 */
N5E.individualTargetTypes = {
  self: "N5E.TargetSelf",
  ally: "N5E.TargetAlly",
  enemy: "N5E.TargetEnemy",
  creature: "N5E.TargetCreature",
  object: "N5E.TargetObject",
  space: "N5E.TargetSpace",
  creatureOrObject: "N5E.TargetCreatureOrObject",
  any: "N5E.TargetAny",
  willing: "N5E.TargetWilling"
};
preLocalize("individualTargetTypes");

/* -------------------------------------------- */

/**
 * Information needed to represent different area of effect target types.
 *
 * @typedef {object} AreaTargetDefinition
 * @property {string} label     Localized label for this type.
 * @property {string} template  Type of `MeasuredTemplate` create for this target type.
 */

/**
 * Targeting types that cover an area.
 * @enum {AreaTargetDefinition}
 */
N5E.areaTargetTypes = {
  radius: {
    label: "N5E.TargetRadius",
    template: "circle"
  },
  sphere: {
    label: "N5E.TargetSphere",
    template: "circle"
  },
  cylinder: {
    label: "N5E.TargetCylinder",
    template: "circle"
  },
  cone: {
    label: "N5E.TargetCone",
    template: "cone"
  },
  square: {
    label: "N5E.TargetSquare",
    template: "rect"
  },
  cube: {
    label: "N5E.TargetCube",
    template: "rect"
  },
  line: {
    label: "N5E.TargetLine",
    template: "ray"
  },
  wall: {
    label: "N5E.TargetWall",
    template: "ray"
  }
};
preLocalize("areaTargetTypes", { key: "label", sort: true });

/* -------------------------------------------- */

/**
 * The types of single or area targets which can be applied to abilities.
 * @enum {string}
 */
N5E.targetTypes = {
  ...N5E.individualTargetTypes,
  ...Object.fromEntries(Object.entries(N5E.areaTargetTypes).map(([k, v]) => [k, v.label]))
};
preLocalize("targetTypes", { sort: true });

/* -------------------------------------------- */

/**
 * Denominations of chakra dice which can apply to classes.
 * @type {string[]}
 */
N5E.chakraDieTypes = ["d6", "d8", "d10", "d12"];

/* -------------------------------------------- */

/**
 * Denominations of hit dice which can apply to classes.
 * @type {string[]}
 */
N5E.hitDieTypes = ["d6", "d8", "d10", "d12"];

/* -------------------------------------------- */

/**
 * The set of possible sensory perception types which an Actor may have.
 * @enum {string}
 */
N5E.senses = {
  blindsight: "N5E.SenseBlindsight",
  chakrasight: "N5E.SenseChakrasight",
  darkvision: "N5E.SenseDarkvision",
  tremorsense: "N5E.SenseTremorsense",
  truesight: "N5E.SenseTruesight"
};
preLocalize("senses", { sort: true });

/* -------------------------------------------- */
/*  Jutsucasting                                */
/* -------------------------------------------- */

/**
 * Valid jutsu ranks.
 * @enum {string}
 */
N5E.jutsuRanks = {
  0: "N5E.JutsuRank0",
  1: "N5E.JutsuRank1",
  2: "N5E.JutsuRank2",
  3: "N5E.JutsuRank3",
  4: "N5E.JutsuRank4",
  5: "N5E.JutsuRank5"
};
preLocalize("jutsuRanks");

/* -------------------------------------------- */

/**
 * The available choices for how jutsu damage scaling may be computed.
 * @enum {string}
 */
N5E.jutsuScalingModes = {
  none: "N5E.JutsuNone",
  level: "N5E.JutsuERank",
  rank: "N5E.JutsuRank"
};
preLocalize("jutsuScalingModes", { sort: true });

/* -------------------------------------------- */

/**
 * Types of components that can be required when casting a jutsu.
 * @enum {object}
 */
N5E.jutsuComponents = {
  handSeals: {
    label: "N5E.ComponentHandSeals",
    abbr: "N5E.ComponentHandSealsAbbr"
  },
  chakraMolding: {
    label: "N5E.ComponentChakraMolding",
    abbr: "N5E.ComponentChakraMoldingAbbr"
  },
  chakraSeals: {
    label: "N5E.ComponentChakraSeals",
    abbr: "N5E.ComponentChakraSealsAbbr"
  },
  mobility: {
    label: "N5E.ComponentMobility",
    abbr: "N5E.ComponentMobilityAbbr"
  },
  weapon: {
    label: "N5E.ComponentWeapon",
    abbr: "N5E.ComponentWeaponAbbr"
  },
  ninjaTool: {
    label: "N5E.ComponentNinjaTool",
    abbr: "N5E.ComponentNinjaToolAbbr"
  }
};
preLocalize("jutsuComponents", {keys: ["label", "abbr"]});

/* -------------------------------------------- */

/**
 * Supplementary rules keywords that inform a jutsu's use.
 * @enum {object}
 */
N5E.jutsuKeywords = {
  ninjutsu: {
    label: "N5E.Ninjutsu",
    abbr: "N5E.NinjutsuAbbr"
  },
  genjutsu: {
    label: "N5E.Genjutsu",
    abbr: "N5E.GenjutsuAbbr"
  },
  taijutsu: {
    label: "N5E.Taijutsu",
    abbr: "N5E.TaijutsuAbbr"
  },
  bukijutsu: {
    label: "N5E.Bukijutsu",
    abbr: "N5E.BukijutsuAbbr"
  },
  hijutsu: {
    label: "N5E.Hijutsu",
    abbr: "N5E.HijutsuAbbr"
  },
  concentration: {
    label: "N5E.Concentration",
    abbr: "N5E.ConcentrationAbbr"
  },
  earthRelease: {
    label: "N5E.EarthRelease",
    abbr: "N5E.EarthReleaseAbbr"
  },
  windRelease: {
    label: "N5E.WindRelease",
    abbr: "N5E.WindReleaseAbbr"
  },
  fireRelease: {
    label: "N5E.FireRelease",
    abbr: "N5E.FireReleaseAbbr"
  },
  waterRelease: {
    label: "N5E.WaterRelease",
    abbr: "N5E.WaterReleaseAbbr"
  },
  lightningRelease: {
    label: "N5E.LightningRelease",
    abbr: "N5E.LightningReleaseAbbr"
  },
  medical: {
    label: "N5E.Medical",
    abbr: "N5E.MedicalAbbr"
  },
  fuinjutsu: {
    label: "N5E.Fuinjutsu",
    abbr: "N5E.FuinjutsuAbbr"
  },
  sensory: {
    label: "N5E.Sensory",
    abbr: "N5E.SensoryAbbr"
  },
  visual: {
    label: "N5E.Visual",
    abbr: "N5E.VisualAbbr"
  },
  auditory: {
    label: "N5E.Auditory",
    abbr: "N5E.AuditoryAbbr"
  },
  inhaled: {
    label: "N5E.Inhaled",
    abbr: "N5E.InhaledAbbr"
  },
  tactile: {
    label: "N5E.Tactile",
    abbr: "N5E.TactileAbbr"
  },
  unaware: {
    label: "N5E.Unaware",
    abbr: "N5E.UnawareAbbr"
  },
  clash: {
    label: "N5E.Clash",
    abbr: "N5E.ClashAbbr"
  },
  combo: {
    label: "N5E.Combo",
    abbr: "N5E.ComboAbbr"
  },
  finisher: {
    label: "N5E.Finisher",
    abbr: "N5E.FinisherAbbr"
  },
  combination: {
    label: "N5E.Combination",
    abbr: "N5E.CombinationAbbr"
  },
  clone: {
    label: "N5E.Clone",
    abbr: "N5E.CloneAbbr"
  }
};
preLocalize("jutsuKeywords", {keys: ["label", "abbr"]});

/* -------------------------------------------- */

/**
 * Classifications to which a jutsu can belong.
 * @enum {string}
 */
N5E.jutsuClasses = {
  nin: "N5E.ClassNin",
  gen: "N5E.ClassGen",
  tai: "N5E.ClassTai",
  bki: "N5E.ClassBki",
  hij: "N5E.ClassHij",
  art: "N5E.ClassArt"
};
preLocalize("jutsuClasses", { sort: true });

/* -------------------------------------------- */

/**
 * Jutsu scroll item ID within the `N5E.sourcePacks` compendium for each rank.
 * @enum {string}
 */
N5E.jutsuScrollIds = {
  0: "FCgmwT7qK7XP3U93",
  1: "eu92JLdFb5nk7oxC",
  2: "9n8zhYcSnLQpdeqz",
  3: "w6ZNZ7pPukblL9KT",
  4: "ae78xByLP7Ipg1qz",
  5: "Df1YUlXrQSagK5px"
};

/* -------------------------------------------- */
/*  Weapon Details                              */
/* -------------------------------------------- */

/**
 * The set of types which a weapon item can take.
 * @enum {string}
 */
N5E.weaponTypes = {
  simpleM: "N5E.WeaponSimpleM",
  simpleR: "N5E.WeaponSimpleR",
  martialM: "N5E.WeaponMartialM",
  martialR: "N5E.WeaponMartialR",
  natural: "N5E.WeaponNatural",
  improv: "N5E.WeaponImprov",
  siege: "N5E.WeaponSiege"
};
preLocalize("weaponTypes");

/* -------------------------------------------- */

/**
 * A subset of weapon properties that determine the physical characteristics of the weapon.
 * These properties are used for determining physical resistance bypasses.
 * @enum {string}
 */
N5E.physicalWeaponProperties = {
  enh: "N5E.WeaponPropertiesEnh"
};
preLocalize("physicalWeaponProperties", { sort: true });

/* -------------------------------------------- */

/**
 * The set of weapon property flags which can exist on a weapon.
 * @enum {string}
 */
N5E.weaponProperties = {
  ...N5E.physicalWeaponProperties,
  amm: "N5E.WeaponPropertiesAmm",
  blc: "N5E.WeaponPropertiesBlc",
  crt: "N5E.WeaponPropertiesCrt",
  ded: "N5E.WeaponPropertiesDed",
  dis: "N5E.WeaponPropertiesDis",
  fin: "N5E.WeaponPropertiesFin",
  gpl: "N5E.WeaponPropertiesGpl",
  hvy: "N5E.WeaponPropertiesHvy",
  hdn: "N5E.WeaponPropertiesHdn",
  lgt: "N5E.WeaponPropertiesLgt",
  lod: "N5E.WeaponPropertiesLod",
  mlt: "N5E.WeaponPropertiesMlt",
  rng: "N5E.WeaponPropertiesRng",
  rch: "N5E.WeaponPropertiesRch",
  ret: "N5E.WeaponPropertiesRet",
  spc: "N5E.WeaponPropertiesSpc",
  thr: "N5E.WeaponPropertiesThr",
  trp: "N5E.WeaponPropertiesTrp",
  two: "N5E.WeaponPropertiesTwo",
  una: "N5E.WeaponPropertiesUna",
  ver: "N5E.WeaponPropertiesVer"
};
preLocalize("weaponProperties", { sort: true });

/* -------------------------------------------- */

/**
 * Compendium packs used for localized items.
 * @enum {string}
 */
N5E.sourcePacks = {
  ITEMS: "n5e.items"
};

/* -------------------------------------------- */

/**
 * Settings to configure how actors are merged when polymorphing is applied.
 * @enum {string}
 */
N5E.polymorphSettings = {
  keepPhysical: "N5E.PolymorphKeepPhysical",
  keepMental: "N5E.PolymorphKeepMental",
  keepSaves: "N5E.PolymorphKeepSaves",
  keepSkills: "N5E.PolymorphKeepSkills",
  mergeSaves: "N5E.PolymorphMergeSaves",
  mergeSkills: "N5E.PolymorphMergeSkills",
  keepClass: "N5E.PolymorphKeepClass",
  keepFeats: "N5E.PolymorphKeepFeats",
  keepJutsu: "N5E.PolymorphKeepJutsu",
  keepItems: "N5E.PolymorphKeepItems",
  keepBio: "N5E.PolymorphKeepBio",
  keepVision: "N5E.PolymorphKeepVision",
  keepSelf: "N5E.PolymorphKeepSelf"
};
preLocalize("polymorphSettings", { sort: true });

/**
 * Settings to configure how actors are effects are merged when polymorphing is applied.
 * @enum {string}
 */
N5E.polymorphEffectSettings = {
  keepAE: "N5E.PolymorphKeepAE",
  keepOtherOriginAE: "N5E.PolymorphKeepOtherOriginAE",
  keepOriginAE: "N5E.PolymorphKeepOriginAE",
  keepEquipmentAE: "N5E.PolymorphKeepEquipmentAE",
  keepFeatAE: "N5E.PolymorphKeepFeatureAE",
  keepJutsuAE: "N5E.PolymorphKeepJutsuAE",
  keepClassAE: "N5E.PolymorphKeepClassAE",
  keepClassModAE: "N5E.PolymorphKeepClassModAE",
  keepClanAE: "N5E.PolymorphKeepClanAE",
  keepBackgroundAE: "N5E.PolymorphKeepBackgroundAE"
};
preLocalize("polymorphEffectSettings", { sort: true });

/**
 * Settings to configure how actors are merged when preset polymorphing is applied.
 * @enum {object}
 */
N5E.transformationPresets = {
  naturalTalent: {
    icon: '<i class="fas fa-paw"></i>',
    label: "N5E.PolymorphNaturalTalent",
    options: {
      keepBio: true,
      keepClass: true,
      keepMental: true,
      mergeSaves: true,
      mergeSkills: true,
      keepEquipmentAE: false
    }
  },
  polymorph: {
    icon: '<i class="fas fa-pastafarianism"></i>',
    label: "N5E.Polymorph",
    options: {
      keepEquipmentAE: false,
      keepClassAE: false,
      keepFeatAE: false,
      keepBackgroundAE: false
    }
  },
  polymorphSelf: {
    icon: '<i class="fas fa-eye"></i>',
    label: "N5E.PolymorphSelf",
    options: {
      keepSelf: true
    }
  }
};
preLocalize("transformationPresets", { sort: true, keys: ["label"] });

/* -------------------------------------------- */

/**
 * Skill, ability, and tool proficiency levels.
 * The key for each level represents its proficiency multiplier.
 * @enum {string}
 */
N5E.proficiencyLevels = {
  0: "N5E.NotProficient",
  1: "N5E.Proficient",
  0.5: "N5E.HalfProficient",
  2: "N5E.Expertise"
};
preLocalize("proficiencyLevels");

/* -------------------------------------------- */

/**
 * Weapon and armor item proficiency levels.
 * @enum {string}
 */
N5E.weaponAndArmorProficiencyLevels = {
  0: "N5E.NotProficient",
  1: "N5E.Proficient"
};
preLocalize("weaponAndArmorProficiencyLevels");

/* -------------------------------------------- */

/**
 * The amount of cover provided by an object. In cases where multiple pieces
 * of cover are in play, we take the highest value.
 * @enum {string}
 */
N5E.cover = {
  0: "N5E.None",
  .5: "N5E.CoverHalf",
  .75: "N5E.CoverThreeQuarters",
  1: "N5E.CoverTotal"
};
preLocalize("cover");

/* -------------------------------------------- */

/**
 * A selection of actor attributes that can be tracked on token resource bars.
 * @type {string[]}
 * @deprecated since v10
 */
N5E.trackableAttributes = [
  "attributes.cp.value", "attributes.ac.value", "attributes.init.bonus", "attributes.movement", "attributes.senses",
  "attributes.genjutsudc", "attributes.ninjutsudc", "attributes.taijutsudc", "details.npcLevel",
  "details.xp.value", "skills.*.passive", "abilities.*.value"
];

/* -------------------------------------------- */

/**
 * A selection of actor and item attributes that are valid targets for item resource consumption.
 * @type {string[]}
 */
N5E.consumableResources = [
  // Configured during init.
];

/* -------------------------------------------- */

/**
 * Conditions that can affect an actor.
 * @enum {string}
 */
N5E.conditionTypes = {
  berserk: "N5E.ConBerserk",
  bleeding: "N5E.ConBleeding",
  blinded: "N5E.ConBlinded",
  bruised: "N5E.ConBruised",
  burned: "N5E.ConBurned",
  charmed: "N5E.ConCharmed",
  chilled: "N5E.ConChilled",
  corroded: "N5E.ConCorroded",
  dazed: "N5E.ConDazed",
  dazzled: "N5E.ConDazzled",
  deafened: "N5E.ConDeafened",
  demoralized: "N5E.ConDemoralized",
  dying: "N5E.ConDying",
  exhaustion: "N5E.ConExhaustion",
  frostbite: "N5E.ConFrostbite",
  galvanized: "N5E.ConGalvanized",
  grappled: "N5E.ConGrappled",
  immolated: "N5E.ConImmolated",
  incapacitated: "N5E.ConIncapacitated",
  invisible: "N5E.ConInvisible",
  lacerated: "N5E.ConLacerated",
  petrified: "N5E.ConPetrified",
  poisoned: "N5E.ConPoisoned",
  prone: "N5E.ConProne",
  restrained: "N5E.ConRestrained",
  sealed: "N5E.ConSealed",
  shocked: "N5E.ConShocked",
  slowed: "N5E.ConSlowed",
  staggered: "N5E.ConStaggered",
  stunned: "N5E.ConStunned",
  unconscious: "N5E.ConUnconscious",
  weakened: "N5E.ConWeakened",
  concentration: "N5E.ConConcentration"
};
preLocalize("conditionTypes", { sort: true });

/**
 * Languages a character can learn.
 * @enum {string}
 */
N5E.languages = {
  cloudDialect: "N5E.LanguagesCloudDialect",
  common: "N5E.LanguagesCommon",
  dogSpeak: "N5E.LanguagesDogSpeak",
  earthDialect: "N5E.LanguagesEarthDialect",
  fireDialect: "N5E.LanguagesFireDialect",
  insectSpeak: "N5E.LanguagesInsectSpeak",
  sandDialect: "N5E.LanguagesSandDialect",
  waterDialect: "N5E.LanguagesWaterDialect"
};
preLocalize("languages", { sort: true });

/**
 * Maximum allowed character level.
 * @type {number}
 */
N5E.maxLevel = 20;

/**
 * Maximum allowed class mod level.
 * @type {number}
 */
N5E.maxClassModLevel = 6;

/**
 * Maximum ability score value allowed by default.
 * @type {number}
 */
N5E.maxAbilityScore = 20;

/**
 * XP required to achieve each character level.
 * @type {number[]}
 */
N5E.CHARACTER_EXP_LEVELS = [
  0, 50, 75, 100, 150, 200, 350, 475, 600, 725, 850, 1000,
  1200, 1400, 1600, 1800, 2100, 2400, 2700, 3000
];

/**
 * @typedef {object} CharacterFlagConfig
 * @property {string} name
 * @property {string} hint
 * @property {string} section
 * @property {typeof boolean|string|number} type
 * @property {string} placeholder
 * @property {string[]} [abilities]
 * @property {Object<string, string>} [choices]
 * @property {string[]} [skills]
 */

/* -------------------------------------------- */

/**
 * Trait configuration information.
 *
 * @typedef {object} TraitConfiguration
 * @property {string} label               Localization key for the trait name.
 * @property {string} [actorKeyPath]      If the trait doesn't directly map to an entry as `traits.[key]`, where is
 *                                        this trait's data stored on the actor?
 * @property {string} [configKey]         If the list of trait options doesn't match the name of the trait, where can
 *                                        the options be found within `CONFIG.N5E`?
 * @property {string} [labelKey]          If config is an enum of objects, where can the label be found?
 * @property {object} [subtypes]          Configuration for traits that take some sort of base item.
 * @property {string} [subtypes.keyPath]  Path to subtype value on base items, should match a category key.
 * @property {string[]} [subtypes.ids]    Key for base item ID objects within `CONFIG.N5E`.
 * @property {object} [children]          Mapping of category key to an object defining its children.
 * @property {boolean} [sortCategories]   Whether top-level categories should be sorted.
 */

/**
 * Configurable traits on actors.
 * @enum {TraitConfiguration}
 */
N5E.traits = {
  saves: {
    label: "N5E.ClassSaves",
    configKey: "abilities",
    labelKey: "label"
  },
  skills: {
    label: "N5E.TraitSkillProf",
    labelKey: "label"
  },
  languages: {
    label: "N5E.Languages"
  },
  di: {
    label: "N5E.DamImm",
    configKey: "damageTypes"
  },
  dr: {
    label: "N5E.DamRes",
    configKey: "damageTypes"
  },
  dv: {
    label: "N5E.DamVuln",
    configKey: "damageTypes"
  },
  ci: {
    label: "N5E.ConImm",
    configKey: "conditionTypes"
  },
  weapon: {
    label: "N5E.TraitWeaponProf",
    actorKeyPath: "traits.weaponProf",
    configKey: "weaponProficiencies",
    subtypes: { keyPath: "weaponType", ids: ["weaponIds"] }
  },
  armor: {
    label: "N5E.TraitArmorProf",
    actorKeyPath: "traits.armorProf",
    configKey: "armorProficiencies",
    subtypes: { keyPath: "armor.type", ids: ["armorIds"] }
  },
  tool: {
    label: "N5E.TraitToolProf",
    actorKeyPath: "tools",
    configKey: "toolProficiencies",
    subtypes: { keyPath: "toolType", ids: ["toolIds"] },
    children: { vehicle: "vehicleTypes" },
    sortCategories: true
  }
};
preLocalize("traits", { key: "label" });

/* -------------------------------------------- */

/**
 * Special character flags.
 * @enum {CharacterFlagConfig}
 */
N5E.characterFlags = {
  initiativeAdv: {
    name: "N5E.FlagsInitiativeAdv",
    hint: "N5E.FlagsInitiativeAdvHint",
    section: "N5E.Feats",
    type: Boolean
  },
  initiativeAlert: {
    name: "N5E.FlagsAlert",
    hint: "N5E.FlagsAlertHint",
    section: "N5E.Feats",
    type: Boolean
  },
  eliteAgility: {
    name: "N5E.FlagsEliteAgility",
    hint: "N5E.FlagsEliteAgilityHint",
    section: "N5E.Feats",
    type: Boolean
  },
  swiftResponse: {
    name: "N5E.FlagsSwiftResponse",
    hint: "N5E.FlagsSwiftResponseHint",
    section: "N5E.Feats",
    type: Boolean
  },
  jackOfAllTrades: {
    name: "N5E.FlagsJOAT",
    hint: "N5E.FlagsJOATHint",
    section: "N5E.Feats",
    type: Boolean
  },
  remarkableAthlete: {
    name: "N5E.FlagsRemarkableAthlete",
    hint: "N5E.FlagsRemarkableAthleteHint",
    abilities: ["str", "dex", "con"],
    section: "N5E.Feats",
    type: Boolean
  },
  weaponCriticalThreshold: {
    name: "N5E.FlagsWeaponCritThreshold",
    hint: "N5E.FlagsWeaponCritThresholdHint",
    section: "N5E.Feats",
    type: Number,
    placeholder: 20
  },
  jutsuCriticalThreshold: {
    name: "N5E.FlagsJutsuCritThreshold",
    hint: "N5E.FlagsJutsuCritThresholdHint",
    section: "N5E.Feats",
    type: Number,
    placeholder: 20
  },
  meleeCriticalDamageDice: {
    name: "N5E.FlagsMeleeCriticalDice",
    hint: "N5E.FlagsMeleeCriticalDiceHint",
    section: "N5E.Feats",
    type: Number,
    placeholder: 0
  }
};
preLocalize("characterFlags", { keys: ["name", "hint", "section"] });

/**
 * Flags allowed on actors. Any flags not in the list may be deleted during a migration.
 * @type {string[]}
 */
N5E.allowedActorFlags = ["isPolymorphed", "originalActor"].concat(Object.keys(N5E.characterFlags));

/* -------------------------------------------- */

/**
 * Advancement types that can be added to items.
 * @enum {*}
 */
N5E.advancementTypes = {
  AbilityScoreImprovement: advancement.AbilityScoreImprovementAdvancement,
  ChakraPoints: advancement.ChakraPointsAdvancement,
  HitPoints: advancement.HitPointsAdvancement,
  ItemChoice: advancement.ItemChoiceAdvancement,
  ItemGrant: advancement.ItemGrantAdvancement,
  ScaleValue: advancement.ScaleValueAdvancement
};

/* -------------------------------------------- */

/**
 * Patch an existing config enum to allow conversion from string values to object values without
 * breaking existing modules that are expecting strings.
 * @param {string} key          Key within N5E that has been replaced with an enum of objects.
 * @param {string} fallbackKey  Key within the new config object from which to get the fallback value.
 * @param {object} [options]    Additional options passed through to logCompatibilityWarning.
 */
function patchConfig(key, fallbackKey, options) {
  /** @override */
  function toString() {
    const message = `The value of CONFIG.N5E.${key} has been changed to an object.`
      +` The former value can be acccessed from .${fallbackKey}.`;
    foundry.utils.logCompatibilityWarning(message, options);
    return this[fallbackKey];
  }

  Object.values(N5E[key]).forEach(o => o.toString = toString);
}

/* -------------------------------------------- */

export default N5E;