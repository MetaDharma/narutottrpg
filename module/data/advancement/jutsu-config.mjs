import { FormulaField } from "../fields.mjs";

export default class JutsuConfigurationData extends foundry.abstract.DataModel {
  /** @inheritdoc */
  static defineSchema() {
    return {
      ability: new foundry.data.fields.StringField({label: "N5E.AbilityModifier"}),
      uses: new foundry.data.fields.SchemaField({
        max: new FormulaField({deterministic: true, label: "N5E.UsesMax"}),
        per: new foundry.data.fields.StringField({label: "N5E.UsesPeriod"})
      }, {label: "N5E.LimitedUses"})
    };
  }

  /* -------------------------------------------- */

  /**
   * Changes that this jutsu configuration indicates should be performed on jutsu.
   * @type {object}
   */
  get jutsuChanges() {
    const updates = {};
    if ( this.ability ) updates["system.ability"] = this.ability;
    if ( this.uses.max && this.uses.per ) {
      updates["system.uses.max"] = this.uses.max;
      updates["system.uses.per"] = this.uses.per;
      if ( Number.isNumeric(this.uses.max) ) updates["system.uses.value"] = parseInt(this.uses.max);
      else {
        try {
          const rollData = this.parent.parent.actor.getRollData({ deterministic: true });
          const formula = Roll.replaceFormulaData(this.uses.max, rollData, {missing: 0});
          updates["system.uses.value"] = Roll.safeEval(formula);
        } catch(e) { }
      }
    }
    return updates;
  }
}