/**
 * A specialized Dialog subclass for ability usage.
 *
 * @param {ItemN5e} item            Item that is being used.
 * @param {object} [dialogData={}]  An object of dialog data which configures how the modal window is rendered.
 * @param {object} [options={}]     Dialog rendering options.
 */
export default class AbilityUseDialog extends Dialog {
  constructor(item, dialogData={}, options={}) {
    super(dialogData, options);
    this.options.classes = ["n5e", "dialog"];

    /**
     * Store a reference to the Item document being used
     * @type {ItemN5e}
     */
    this.item = item;
  }

  /* -------------------------------------------- */
  /*  Rendering                                   */
  /* -------------------------------------------- */

  /**
   * A constructor function which displays the Jutsu Cast Dialog app for a given Actor and Item.
   * Returns a Promise which resolves to the dialog FormData once the workflow has been completed.
   * @param {ItemN5e} item  Item being used.
   * @returns {Promise}     Promise that is resolved when the use dialog is acted upon.
   */
  static async create(item) {
    if ( !item.isOwned ) throw new Error("You cannot display an ability usage dialog for an unowned item");

    // Prepare data
    const uses = item.system.uses ?? {};
    const chakra = item.system.chakraConsume ?? {};
    const resource = item.system.consume ?? {};
    const quantity = item.system.quantity ?? 0;
    const recharge = item.system.recharge ?? {};
    const recharges = !!recharge.value;
    const sufficientUses = (quantity > 0 && !uses.value) || uses.value > 0;

    // Prepare dialog form data
    const data = {
      item: item,
      title: game.i18n.format("N5E.AbilityUseHint", {type: game.i18n.localize(CONFIG.Item.typeLabels[item.type]), name: item.name}),
      note: this._getAbilityUseNote(item, uses, recharge),
      consumeChakra: chakra.target && (item.type === "jutsu") && (!item.hasAttack || (resource.type !== "ammo")),
      featureChakra: chakra.feature && (!item.hasAttack || (resource.type !== "ammo")),
      consumeRecharge: recharges,
      consumeResource: resource.target && (!item.hasAttack || (resource.type !== "ammo")),
      consumeUses: uses.per && (uses.max > 0),
      canUse: recharges ? recharge.charged : sufficientUses,
      createTemplate: game.user.can("TEMPLATE_CREATE") && item.hasAreaTarget,
      errors: []
    };
    if ( item.type === "jutsu" ) this._getJutsuData(item.actor.system, item.system, data);

    // Render the ability usage template
    const html = await renderTemplate("systems/n5e/templates/apps/ability-use.hbs", data);

    // Create the Dialog and return data as a Promise
    const icon = data.isJutsu ? "fa-magic" : "fa-fist-raised";
    const label = game.i18n.localize(`N5E.AbilityUse${data.isJutsu ? "Cast" : "Use"}`);
    return new Promise(resolve => {
      const dlg = new this(item, {
        title: `${item.name}: ${game.i18n.localize("N5E.AbilityUseConfig")}`,
        content: html,
        buttons: {
          use: {
            icon: `<i class="fas ${icon}"></i>`,
            label: label,
            callback: html => {
              const fd = new FormDataExtended(html[0].querySelector("form"));
              resolve(fd.object);
            }
          }
        },
        default: "use",
        close: () => resolve(null)
      });
      dlg.render(true);
    });
  }

  /* -------------------------------------------- */
  /*  Helpers                                     */
  /* -------------------------------------------- */

  /**
   * Get dialog data related to chakra.
   * @param {object} actorData  System data from the actor using the jutsu.
   * @param {object} itemData   System data from the jutsu being used.
   * @param {object} data       Data for the dialog being presented.
   * @returns {object}          Modified dialog data.
   * @private
   */
  static _getJutsuData(actorData, itemData, data) {

    // Determine whether the jutsu may be up-cast
    let rnk = itemData.rank;
    let upc = itemData.upcastCost;
    let bsr = itemData.baseRank;
    let chakraCost = itemData.chakraConsume.amount;
    const requireChakra = (chakraCost > 0);
    let jutsuRanks;

    // If can't upcast, return early and don't bother calculating available chakra
    if ( !requireChakra ) {
      return foundry.utils.mergeObject(data, { isJutsu: true, requireChakra });
    }

    // Determine the ranks which are feasible
    let rmax = 0;
    if ( upc === 0 ){
      jutsuRanks = Array.fromRange(1).reduce((arr, i) => {
        if ( rnk < rnk ) return arr;
        const label = CONFIG.N5E.jutsuRanks[bsr];
        let max = 6;
        let chakra = actorData.attributes.cp.value;
  
        if ( max > 0 ) {
          rmax = i;
          chakraCost = itemData.chakraConsume.amount + ((itemData.upcastCost * i) - (itemData.upcastCost * rnk));
        } arr.push({
          rank: i,
          label: i >= 0 ? game.i18n.format("N5E.JutsuRankChakra", {rank: label, n: chakraCost}) : label,
          canCast: chakra >= chakraCost,
          hasChakra: chakra > 0
        });
        return arr;
      }, []);
    } else {
      jutsuRanks = Array.fromRange(Object.keys(CONFIG.N5E.jutsuRanks).length).reduce((arr, i) => {
        if ( i < rnk ) return arr;
        const label = CONFIG.N5E.jutsuRanks[i];
        let max = 6;
        let chakra = actorData.attributes.cp.value;
  
        if ( max > 0 ) {
          rmax = i;
          chakraCost = itemData.chakraConsume.amount + ((itemData.upcastCost * i) - (itemData.upcastCost * rnk));
        } arr.push({
          rank: i,
          label: i >= 0 ? game.i18n.format("N5E.JutsuRankChakra", {rank: label, n: chakraCost}) : label,
          canCast: chakra >= chakraCost,
          hasChakra: chakra > 0
        });
        return arr;
      }, []);
    }

    const canCast = actorData.attributes.cp.value >= itemData.chakraConsume.amount;
    if ( !canCast ) data.errors.push(game.i18n.format("N5E.JutsuCastNoChakra", {
      name: data.item.name
    }));

    // Merge jutsu casting data
    return foundry.utils.mergeObject(data, { isJutsu: true, requireChakra, jutsuRanks });
  }

  /* -------------------------------------------- */

  /**
   * Get the ability usage note that is displayed.
   * @param {object} item                                     Data for the item being used.
   * @param {{value: number, max: number, per: string}} uses  Object uses and recovery configuration.
   * @param {{charged: boolean, value: string}} recharge      Object recharge configuration.
   * @returns {string}                                        Localized string indicating available uses.
   * @private
   */
  static _getAbilityUseNote(item, uses, recharge) {

    // Zero quantity
    const quantity = item.system.quantity;
    if ( quantity <= 0 ) return game.i18n.localize("N5E.AbilityUseUnavailableHint");

    // Abilities which use Recharge
    if ( recharge.value ) {
      return game.i18n.format(recharge.charged ? "N5E.AbilityUseChargedHint" : "N5E.AbilityUseRechargeHint", {
        type: game.i18n.localize(CONFIG.Item.typeLabels[item.type])
      });
    }

    // Does not use any resource
    if ( !uses.per || !uses.max ) return "";

    // Consumables
    if ( item.type === "consumable" ) {
      let str = "N5E.AbilityUseNormalHint";
      if ( uses.value > 1 ) str = "N5E.AbilityUseConsumableChargeHint";
      else if ( item.system.quantity === 1 && uses.autoDestroy ) str = "N5E.AbilityUseConsumableDestroyHint";
      else if ( item.system.quantity > 1 ) str = "N5E.AbilityUseConsumableQuantityHint";
      return game.i18n.format(str, {
        type: game.i18n.localize(`N5E.Consumable${item.system.consumableType.capitalize()}`),
        value: uses.value,
        quantity: item.system.quantity,
        max: uses.max,
        per: CONFIG.N5E.limitedUsePeriods[uses.per]
      });
    }

    // Other Items
    else {
      return game.i18n.format("N5E.AbilityUseNormalHint", {
        type: game.i18n.localize(CONFIG.Item.typeLabels[item.type]),
        value: uses.value,
        max: uses.max,
        per: CONFIG.N5E.limitedUsePeriods[uses.per]
      });
    }
  }
}