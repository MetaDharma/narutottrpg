import Advancement from "./advancement.mjs";
import ChakraPointsConfig from "../../applications/advancement/chakra-points-config.mjs";
import ChakraPointsFlow from "../../applications/advancement/chakra-points-flow.mjs";
import { simplifyBonus } from "../../utils.mjs";

/**
 * Advancement that presents the player with the option to roll chakra points at each level or select the average value.
 * Keeps track of player chakra point rolls or selection for each class level. **Can only be added to classes and each
 * class can only have one.**
 */
export default class ChakraPointsAdvancement extends Advancement {

  /** @inheritdoc */
  static get metadata() {
    return foundry.utils.mergeObject(super.metadata, {
      order: 10,
      icon: "systems/n5e/icons/svg/chakra-points.svg",
      title: game.i18n.localize("N5E.AdvancementChakraPointsTitle"),
      hint: game.i18n.localize("N5E.AdvancementChakraPointsHint"),
      multiLevel: true,
      validItemTypes: new Set(["class"]),
      apps: {
        config: ChakraPointsConfig,
        flow: ChakraPointsFlow
      }
    });
  }

  /* -------------------------------------------- */
  /*  Instance Properties                         */
  /* -------------------------------------------- */

  /** @inheritdoc */
  get levels() {
    return Array.fromRange(CONFIG.N5E.maxLevel + 1).slice(1);
  }

  /* -------------------------------------------- */

  /**
   * Shortcut to the chakra die used by the class.
   * @returns {string}
   */
  get chakraDie() {
    return this.item.system.chakraDice;
  }

  /* -------------------------------------------- */

  /**
   * The face value of the chakra die used.
   * @returns {number}
   */
  get chakraDieValue() {
    return Number(this.chakraDie.substring(1));
  }

  /* -------------------------------------------- */
  /*  Display Methods                             */
  /* -------------------------------------------- */

  /** @inheritdoc */
  configuredForLevel(level) {
    return this.valueForLevel(level) !== null;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  titleForLevel(level, { configMode=false }={}) {
    const cp = this.valueForLevel(level);
    if ( !cp || configMode ) return this.title;
    return `${this.title}: <strong>${cp}</strong>`;
  }

  /* -------------------------------------------- */

  /**
   * Chakra points given at the provided level.
   * @param {number} level   Level for which to get chakra points.
   * @returns {number|null}  Chakra points for level or null if none have been taken.
   */
  valueForLevel(level) {
    return this.constructor.valueForLevel(this.value, this.chakraDieValue, level);
  }

  /* -------------------------------------------- */

  /**
   * Chakra points given at the provided level.
   * @param {object} data            Contents of `value` used to determine this value.
   * @param {number} chakraDieValue  Face value of the chakra die used by this advancement.
   * @param {number} level           Level for which to get chakra points.
   * @returns {number|null}          Chakra points for level or null if none have been taken.
   */
  static valueForLevel(data, chakraDieValue, level) {
    const value = data[level];
    if ( !value ) return null;

    if ( value === "max" ) return chakraDieValue;
    if ( value === "avg" ) return (chakraDieValue / 2) + 1;
    return value;
  }

  /* -------------------------------------------- */

  /**
   * Total chakra points provided by this advancement.
   * @returns {number}  Chakra points currently selected.
   */
  total() {
    return Object.keys(this.value).reduce((total, level) => total + this.valueForLevel(parseInt(level)), 0);
  }

  /* -------------------------------------------- */

  /**
   * Total chakra points taking the provided ability modifier into account, with a minimum of 1 per level.
   * @param {number} mod  Modifier to add per level.
   * @returns {number}    Total chakra points plus modifier.
   */
  getAdjustedTotal(mod) {
    return Object.keys(this.value).reduce((total, level) => {
      return total + Math.max(this.valueForLevel(parseInt(level)) + mod, 1);
    }, 0);
  }

  /* -------------------------------------------- */
  /*  Editing Methods                             */
  /* -------------------------------------------- */

  /** @inheritdoc */
  static availableForItem(item) {
    return !item.advancement.byType.ChakraPoints?.length;
  }

  /* -------------------------------------------- */
  /*  Application Methods                         */
  /* -------------------------------------------- */

  /**
   * Add the ability modifier and any bonuses to the provided chakra points value to get the number to apply.
   * @param {number} value  Chakra points taken at a given level.
   * @returns {number}      Chakra points adjusted with ability modifier and per-level bonuses.
   */
  #getApplicableValue(value) {
    const abilityId = CONFIG.N5E.chakraPointsAbility || "con";
    value = Math.max(value + (this.actor.system.abilities[abilityId]?.mod ?? 0), 1);
    value += simplifyBonus(this.actor.system.attributes.cp.bonuses.level, this.actor.getRollData());
    return value;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  apply(level, data) {
    let value = this.constructor.valueForLevel(data, this.chakraDieValue, level);
    if ( value === undefined ) return;
    this.actor.updateSource({
      "system.attributes.cp.value": this.actor.system.attributes.cp.value + this.#getApplicableValue(value)
    });
    this.updateSource({ value: data });
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  restore(level, data) {
    this.apply(level, data);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  reverse(level) {
    let value = this.valueForLevel(level);
    if ( value === undefined ) return;
    this.actor.updateSource({
      "system.attributes.cp.value": this.actor.system.attributes.cp.value - this.#getApplicableValue(value)
    });
    const source = { [level]: this.value[level] };
    this.updateSource({ [`value.-=${level}`]: null });
    return source;
  }
}