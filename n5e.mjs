// Import Configuration
import N5E from "./module/config.mjs";
import registerSystemSettings from "./module/settings.mjs";

// Import Submodules
import * as applications from "./module/applications/_module.mjs";
import * as canvas from "./module/canvas/_module.mjs";
import * as dataModels from "./module/data/_module.mjs";
import * as dice from "./module/dice/_module.mjs";
import * as documents from "./module/documents/_module.mjs";
import * as migrations from "./module/migration.mjs";
import * as utils from "./module/utils.mjs";
import {ModuleArt} from "./module/module-art.mjs";

/* -------------------------------------------- */
/*  Define Module Structure                     */
/* -------------------------------------------- */

globalThis.n5e = {
  applications,
  canvas,
  config: N5E,
  dataModels,
  dice,
  documents,
  migrations,
  utils
};

/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */

Hooks.once("init", function() {
  globalThis.n5e = game.n5e = Object.assign(game.system, globalThis.n5e);
  console.log(`N5E | Initializing the N5e Game System - Version ${n5e.version}\n${N5E.ASCII}`);

  // Record Configuration Values
  CONFIG.N5E = N5E;
  CONFIG.ActiveEffect.documentClass = documents.ActiveEffectN5e;
  CONFIG.Actor.documentClass = documents.ActorN5e;
  CONFIG.Item.documentClass = documents.ItemN5e;
  CONFIG.Token.documentClass = documents.TokenDocumentN5e;
  CONFIG.Token.objectClass = canvas.TokenN5e;
  CONFIG.time.roundTime = 6;
  CONFIG.Dice.DamageRoll = dice.DamageRoll;
  CONFIG.Dice.D20Roll = dice.D20Roll;
  CONFIG.MeasuredTemplate.defaults.angle = 53.13; // Cone RAW should be 53.13 degrees
  CONFIG.ui.combat = applications.combat.CombatTrackerN5e;
  CONFIG.compatibility.excludePatterns.push(/\bActiveEffectN5e#label\b/); // backwards compatibility with v10
  game.n5e.isV10 = game.release.generation < 11;

  // Register System Settings
  registerSystemSettings();

  // Validation strictness.
  if ( game.n5e.isV10 ) _determineValidationStrictness();

  // Configure module art.
  game.n5e.moduleArt = new ModuleArt();

  // Remove honor & sanity from configuration if they aren't enabled
  if ( !game.settings.get("n5e", "honorScore") ) delete N5E.abilities.hon;
  if ( !game.settings.get("n5e", "sanityScore") ) delete N5E.abilities.san;

  // Configure trackable & consumable attributes.
  _configureTrackableAttributes();
  _configureConsumableAttributes();

  // Patch Core Functions
  Combatant.prototype.getInitiativeRoll = documents.combat.getInitiativeRoll;

  // Register Roll Extensions
  CONFIG.Dice.rolls.push(dice.D20Roll);
  CONFIG.Dice.rolls.push(dice.DamageRoll);

  // Hook up system data types
  const modelType = game.n5e.isV10 ? "systemDataModels" : "dataModels";
  CONFIG.Actor[modelType] = dataModels.actor.config;
  CONFIG.Item[modelType] = dataModels.item.config;
  CONFIG.JournalEntryPage[modelType] = dataModels.journal.config;

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("n5e", applications.actor.ActorSheetN5eCharacter, {
    types: ["character"],
    makeDefault: true,
    label: "N5E.SheetClassCharacter"
  });
  Actors.registerSheet("n5e", applications.actor.ActorSheetN5eNPC, {
    types: ["npc"],
    makeDefault: true,
    label: "N5E.SheetClassNPC"
  });
  Actors.registerSheet("n5e", applications.actor.ActorSheetN5eVehicle, {
    types: ["vehicle"],
    makeDefault: true,
    label: "N5E.SheetClassVehicle"
  });
  Actors.registerSheet("n5e", applications.actor.GroupActorSheet, {
    types: ["group"],
    makeDefault: true,
    label: "N5E.SheetClassGroup"
  });

  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("n5e", applications.item.ItemSheetN5e, {
    makeDefault: true,
    label: "N5E.SheetClassItem"
  });
  DocumentSheetConfig.registerSheet(JournalEntryPage, "n5e", applications.journal.JournalClassPageSheet, {
    label: "N5E.SheetClassClassSummary",
    types: ["class"]
  });

  // Preload Handlebars helpers & partials
  utils.registerHandlebarsHelpers();
  utils.preloadHandlebarsTemplates();
});

/**
 * Determine if this is a 'legacy' world with permissive validation, or one where strict validation is enabled.
 * @internal
 */
function _determineValidationStrictness() {
  dataModels.SystemDataModel._enableV10Validation = game.settings.get("n5e", "strictValidation");
}

/**
 * Update the world's validation strictness setting based on whether validation errors were encountered.
 * @internal
 */
async function _configureValidationStrictness() {
  if ( !game.user.isGM ) return;
  const invalidDocuments = game.actors.invalidDocumentIds.size + game.items.invalidDocumentIds.size
    + game.scenes.invalidDocumentIds.size;
  const strictValidation = game.settings.get("n5e", "strictValidation");
  if ( invalidDocuments && strictValidation ) {
    await game.settings.set("n5e", "strictValidation", false);
    game.socket.emit("reload");
    foundry.utils.debouncedReload();
  }
}

/**
 * Configure explicit lists of attributes that are trackable on the token HUD and in the combat tracker.
 * @internal
 */
function _configureTrackableAttributes() {
  const common = {
    bar: [],
    value: [
      ...Object.keys(N5E.abilities).map(ability => `abilities.${ability}.value`),
      ...Object.keys(N5E.movementTypes).map(movement => `attributes.movement.${movement}`),
      "attributes.ac.value", "attributes.init.total"
    ]
  };

  const creature = {
    bar: [...common.bar, "attributes.cp", "attributes.hp"],
    value: [
      ...common.value,
      ...Object.keys(N5E.skills).map(skill => `skills.${skill}.passive`),
      ...Object.keys(N5E.senses).map(sense => `attributes.senses.${sense}`),
      "attributes.genjutsudc", "attributes.ninjutsudc", "attributes.taijutsudc"
    ]
  };

  CONFIG.Actor.trackableAttributes = {
    character: {
      bar: [...creature.bar, "resources.primary", "resources.secondary", "resources.tertiary", "details.xp"],
      value: [...creature.value]
    },
    npc: {
      bar: [...creature.bar, "resources.litact", "resources.litres"],
      value: [...creature.value, "details.npcLevel", "details.xp.value"]
    },
    vehicle: {
      bar: [...common.bar, "attributes.hp"],
      value: [...common.value]
    },
    group: {
      bar: [],
      value: []
    }
  };
}

/**
 * Configure which attributes are available for item consumption.
 * @internal
 */
function _configureConsumableAttributes() {
  CONFIG.N5E.consumableResources = [
    ...Object.keys(N5E.abilities).map(ability => `abilities.${ability}.value`),
    "attributes.ac.flat",
    "attributes.cp.value",
    "attributes.hp.value",
    ...Object.keys(N5E.senses).map(sense => `attributes.senses.${sense}`),
    ...Object.keys(N5E.movementTypes).map(type => `attributes.movement.${type}`),
    ...Object.keys(N5E.currencies).map(denom => `currency.${denom}`),
    "details.xp.value",
    "resources.primary.value", "resources.secondary.value", "resources.tertiary.value",
    "resources.litact.value", "resources.litres.value",
    ...Array.fromRange(Object.keys(N5E.jutsuRanks).length - 1, 1).map(rank => `jutsu.jutsu${rank}.value`)
  ];
}

/* -------------------------------------------- */
/*  Foundry VTT Setup                           */
/* -------------------------------------------- */

/**
 * Prepare attribute lists.
 */
Hooks.once("setup", function() {
  CONFIG.N5E.trackableAttributes = expandAttributeList(CONFIG.N5E.trackableAttributes);
  game.n5e.moduleArt.registerModuleArt();

  // Apply custom compendium styles to the Shinobi rules compendium.
  if ( !game.n5e.isV10 ) {
    const rules = game.packs.get("n5e.rules");
    rules.applicationClass = applications.journal.SCompendium;
  }
});

/* --------------------------------------------- */

/**
 * Expand a list of attribute paths into an object that can be traversed.
 * @param {string[]} attributes  The initial attributes configuration.
 * @returns {object}  The expanded object structure.
 */
function expandAttributeList(attributes) {
  return attributes.reduce((obj, attr) => {
    foundry.utils.setProperty(obj, attr, true);
    return obj;
  }, {});
}

/* --------------------------------------------- */

/**
 * Perform one-time pre-localization and sorting of some configuration objects
 */
Hooks.once("i18nInit", () => utils.performPreLocalization(CONFIG.N5E));

/* -------------------------------------------- */
/*  Foundry VTT Ready                           */
/* -------------------------------------------- */

/**
 * Once the entire VTT framework is initialized, check to see if we should perform a data migration
 */
Hooks.once("ready", function() {
  if ( game.n5e.isV10 ) {
    // Configure validation strictness.
    _configureValidationStrictness();

    // Apply custom compendium styles to the Shinobi rules compendium.
    const rules = game.packs.get("n5e.rules");
    rules.apps = [new applications.journal.SCompendium(rules)];
  }

  // Wait to register hotbar drop hook on ready so that modules could register earlier if they want to
  Hooks.on("hotbarDrop", (bar, data, slot) => {
    if ( ["Item", "ActiveEffect"].includes(data.type) ) {
      documents.macro.createN5eMacro(data, slot);
      return false;
    }
  });

  // Determine whether a system migration is required and feasible
  if ( !game.user.isGM ) return;
  const cv = game.settings.get("n5e", "systemMigrationVersion") || game.world.flags.n5e?.version;
  const totalDocuments = game.actors.size + game.scenes.size + game.items.size;
  if ( !cv && totalDocuments === 0 ) return game.settings.set("n5e", "systemMigrationVersion", game.system.version);
  if ( cv && !isNewerVersion(game.system.flags.needsMigrationVersion, cv) ) return;

  // Perform the migration
  if ( cv && isNewerVersion(game.system.flags.compatibleMigrationVersion, cv) ) {
    ui.notifications.error(game.i18n.localize("MIGRATION.N5eVersionTooOldWarning"), {permanent: true});
  }
  migrations.migrateWorld();
});

/* -------------------------------------------- */
/*  Canvas Initialization                       */
/* -------------------------------------------- */

Hooks.on("canvasInit", gameCanvas => {
  gameCanvas.grid.diagonalRule = game.settings.get("n5e", "diagonalMovement");
  SquareGrid.prototype.measureDistances = canvas.measureDistances;
});

/* -------------------------------------------- */
/*  Other Hooks                                 */
/* -------------------------------------------- */

Hooks.on("renderChatMessage", documents.chat.onRenderChatMessage);
Hooks.on("getChatLogEntryContext", documents.chat.addChatMessageContextOptions);

Hooks.on("renderChatLog", (app, html, data) => documents.ItemN5e.chatListeners(html));
Hooks.on("renderChatPopout", (app, html, data) => documents.ItemN5e.chatListeners(html));
Hooks.on("getActorDirectoryEntryContext", documents.ActorN5e.addDirectoryContextOptions);

/* -------------------------------------------- */
/*  Bundled Module Exports                      */
/* -------------------------------------------- */

export {
  applications,
  canvas,
  dataModels,
  dice,
  documents,
  migrations,
  utils,
  N5E
};