System file to be used with Condition Lab & Triggler to add N5E's condtiions to foundry
Copy to modules\condition-lab-triggler\condition-maps\
Upon first copying over this system file you must open Condition Lab and click Restore Defaults
For full functionality many conditions use active effects requiring the N5e rewrite modules [Dynamic Active Effects(N5e)](https://gitlab.com/MetaDharma/daen5e) and [Midi-QOL(N5e)](https://gitlab.com/MetaDharma/midi-qol-n5e)