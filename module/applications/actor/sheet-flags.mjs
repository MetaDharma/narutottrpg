import BaseConfigSheet from "./base-config.mjs";

/**
 * An application class which provides advanced configuration for special character flags which modify an Actor.
 */
export default class ActorSheetFlags extends BaseConfigSheet {

  /** @inheritDoc */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      id: "actor-flags",
      classes: ["n5e"],
      template: "systems/n5e/templates/apps/actor-flags.hbs",
      width: 500,
      closeOnSubmit: true
    });
  }

  /* -------------------------------------------- */

  /** @inheritDoc */
  get title() {
    return `${game.i18n.localize("N5E.FlagsTitle")}: ${this.object.name}`;
  }

  /* -------------------------------------------- */

  /** @inheritDoc */
  getData() {
    const data = {};
    data.actor = this.object;
    data.classes = this._getClasses();
    data.classmods = this._getClassMods();
    data.clans = this._getClans();
    data.flags = this._getFlags();
    data.bonuses = this._getBonuses();
    return data;
  }

  /* -------------------------------------------- */

  /**
   * Prepare an object of sorted classes.
   * @returns {object}
   * @private
   */
  _getClasses() {
    const classes = this.object.items.filter(i => i.type === "class");
    return classes.sort((a, b) => a.name.localeCompare(b.name)).reduce((obj, i) => {
      obj[i.id] = i.name;
      return obj;
    }, {});
  }

  /* -------------------------------------------- */

  /**
   * Prepare an object of sorted class mods.
   * @returns {object}
   * @private
   */
  _getClassMods() {
    const classmods = this.object.items.filter(i => i.type === "classmod");
    return classmods.sort((a, b) => a.name.localeCompare(b.name)).reduce((obj, i) => {
      obj[i.id] = i.name;
      return obj;
    }, {});
  }

  /* -------------------------------------------- */

  /**
   * Prepare an object of sorted clans.
   * @returns {object}
   * @private
   */
  _getClans() {
    const clans = this.object.items.filter(i => i.type === "clan");
    return clans.sort((a, b) => a.name.localeCompare(b.name)).reduce((obj, i) => {
      obj[i.id] = i.name;
      return obj;
    }, {});
  }

  /* -------------------------------------------- */

  /**
   * Prepare an object of flags data which groups flags by section
   * Add some additional data for rendering
   * @returns {object}
   * @private
   */
  _getFlags() {
    const flags = {};
    const baseData = this.document.toJSON();
    for ( let [k, v] of Object.entries(CONFIG.N5E.characterFlags) ) {
      if ( !flags.hasOwnProperty(v.section) ) flags[v.section] = {};
      let flag = foundry.utils.deepClone(v);
      flag.type = v.type.name;
      flag.isCheckbox = v.type === Boolean;
      flag.isSelect = v.hasOwnProperty("choices");
      flag.value = foundry.utils.getProperty(baseData.flags, `n5e.${k}`);
      flags[v.section][`flags.n5e.${k}`] = flag;
    }
    return flags;
  }

  /* -------------------------------------------- */

  /**
   * Get the bonuses fields and their localization strings
   * @returns {Array<object>}
   * @private
   */
  _getBonuses() {
    const src = this.object.toObject();
    const bonuses = [
      {name: "system.bonuses.mwak.attack", label: "N5E.BonusMWAttack"},
      {name: "system.bonuses.mwak.damage", label: "N5E.BonusMWDamage"},
      {name: "system.bonuses.rwak.attack", label: "N5E.BonusRWAttack"},
      {name: "system.bonuses.rwak.damage", label: "N5E.BonusRWDamage"},
      {name: "system.bonuses.mnak.attack", label: "N5E.BonusMNAttack"},
      {name: "system.bonuses.mnak.damage", label: "N5E.BonusMNDamage"},
      {name: "system.bonuses.rnak.attack", label: "N5E.BonusRNAttack"},
      {name: "system.bonuses.rnak.damage", label: "N5E.BonusRNDamage"},
      {name: "system.bonuses.mgak.attack", label: "N5E.BonusMGAttack"},
      {name: "system.bonuses.mgak.damage", label: "N5E.BonusMGDamage"},
      {name: "system.bonuses.rgak.attack", label: "N5E.BonusRGAttack"},
      {name: "system.bonuses.rgak.damage", label: "N5E.BonusRGDamage"},
      {name: "system.bonuses.mtak.attack", label: "N5E.BonusMTAttack"},
      {name: "system.bonuses.mtak.damage", label: "N5E.BonusMTDamage"},
      {name: "system.bonuses.rtak.attack", label: "N5E.BonusRTAttack"},
      {name: "system.bonuses.rtak.damage", label: "N5E.BonusRTDamage"},
      {name: "system.bonuses.abilities.check", label: "N5E.BonusAbilityCheck"},
      {name: "system.bonuses.abilities.save", label: "N5E.BonusAbilitySave"},
      {name: "system.bonuses.abilities.skill", label: "N5E.BonusAbilitySkill"},
      {name: "system.bonuses.jutsu.dc", label: "N5E.BonusJutsuDC"}
    ];
    for ( let b of bonuses ) {
      b.value = foundry.utils.getProperty(src, b.name) || "";
    }
    return bonuses;
  }

  /* -------------------------------------------- */

  /** @inheritDoc */
  async _updateObject(event, formData) {
    const actor = this.object;
    let updateData = foundry.utils.expandObject(formData);
    const src = actor.toObject();

    // Unset any flags which are "false"
    const flags = updateData.flags.n5e;
    for ( let [k, v] of Object.entries(flags) ) {
      if ( [undefined, null, "", false, 0].includes(v) ) {
        delete flags[k];
        if ( foundry.utils.hasProperty(src.flags, `n5e.${k}`) ) flags[`-=${k}`] = null;
      }
    }

    // Clear any bonuses which are whitespace only
    for ( let b of Object.values(updateData.system.bonuses ) ) {
      for ( let [k, v] of Object.entries(b) ) {
        b[k] = v.trim();
      }
    }

    // Diff the data against any applied overrides and apply
    await actor.update(updateData, {diff: false});
  }
}